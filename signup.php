<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #main {
            width: 400px;
            border: 2px solid #4d7aa2;
            border-radius: 8px;
            padding: 32px 80px;
            display: block;

            margin: auto;
            /* box-sizing: border-box; */


            /* height: 400px; */
        }

        #main .wrapper {

            width: 100%;

            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .wrapper .time {
            background-color: #ddd;
            padding: 4px 0;
            width: 100%;
        }

        .time p {
            margin: 8px;
        }

        form {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 16px;
            /* margin-bottom: 32px; */
        }

        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;

        }

        .form-group .form-label {

            width: 30%;
            text-align: center;
            background-color: #5b9bd5;
            padding: 12px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 16px;
            color: #fff;


        }

        .form-group .form-input {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
            flex: 1 1 0;
            /* line-height: 32px; */

        }



        .form-group .form-input input[type="text"] {
            width: 100%;
            font-size: 24px;
            padding: 8px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        .form-group .form-input input[type="radio"] {
            /* width: 30%; */
            /* font-size: 12px; */

            border: 1px solid #4d7aa2;
            background-color: #5b9bd5;
        }

        .form-group .form-input .radio-label {
            margin-left: 8px;
        }

        .form-group .form-input #falcuties {
            width: 100%;
            font-size: 20px;
            padding: 10px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        .form-group .form-input #falcuties:focus,
        .form-group .form-input input[type="text"]:focus {
            border: 2px solid #4d7aa2;
        }

        .form-group .form-input .radio-group {
            display: flex;
            flex-direction: row;
            margin-right: 24px;
        }

        .form-group .form-input .radio-group label {
            font-size: 20px;
        }

        form .submit-btn {
            text-align: center;
            width: 120px;
            color: #fff;
            background-color: #70ad47;
            font-size: 14px;
            padding: 12px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
        }
    </style>
</head>

<body>
    <div id="main">
        <div class="wrapper">
            <form action="" method="post">
                <div class="form-group">
                    <label class="form-label">Họ và tên</label>
                    <div class="form-input">
                        <input type="text" name="username" id="">
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính</label>
                    <div class="form-input">
                        <?php
                        $gender = array("0" => "Nam", "1" => "Nữ");
                        for ($i = 0; $i < count($gender); $i++) {
                            echo '  <div class="radio-group">
                                        <input type="radio" id="' . $gender[$i] . '"  name="gender" value=" ' . $i . '">
                                        <label class="radio-label" for="' . $gender[$i] . '">' . $gender[$i] . '</label>
                                    </div>';
                        }
                        ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <?php
                            $falcuty = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <button type="submit" class="submit-btn">Đăng kí</button>
            </form>
        </div>
    </div>
</body>

</html>